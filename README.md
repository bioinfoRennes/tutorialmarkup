This project contains template files for the class on markup-related languages.

Content :

- `lorem-ipsum.txt` [Lorem ipsum](https://en.wikipedia.org/wiki/Lorem_ipsum) is a classical meaningless placehodler text. There are many generators. This file is for being able to work offline.
- `html`: HTML and XHTML templates
- `LaTeX`: LaTeX article and Beamer presentation. bibtex is for storing bibliographic references.
